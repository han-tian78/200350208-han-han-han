import pytest
import allure
from calculator import add

@allure.step('开始计算')
def begin_calculate():
    print('开始计算')

@allure.step('结束计算')
def end_calculate():
    print('结束计算')

@allure.step('结束测试')
def end_test():
    print('结束测试')

@pytest.fixture(scope='session', autouse=True)
def setup():
    begin_calculate()
    yield
    end_test()


def assert_that(result, param):
    pass


def equal_to(param):
    pass


@allure.feature('计算器')
@allure.tag('hebeu')
class TestCalculator:

    @allure.story('加法')
    def test_addition(self):
        with allure.step('输入两个整数'):
            a = 3
            b = 5
            allure.attach(f'a={a}, b={b}', '输入参数')
        with allure.step('执行加法计算'):
            result = add(a, b)
            allure.attach(f'result={result}', '计算结果')
        with allure.step('断言计算结果'):
            assert_that(result, equal_to(8))

    @allure.story('加法')
    def test_addition_with_negative_numbers(self):
        with allure.step('输入一个正整数和一个负整数'):
            a = 10
            b = -2
            allure.attach(f'a={a}, b={b}', '输入参数')
        with allure.step('执行加法计算'):
            result = add(a, b)
            allure.attach(f'result={result}', '计算结果')
        with allure.step('断言计算结果'):
            assert_that(result, equal_to(8))

    @allure.story('加法')
    def test_addition_with_zero(self):
        with allure.step('输入一个整数和0'):
            a = 5
            b = 0
            allure.attach(f'a={a}, b={b}', '输入参数')
        with allure.step('执行加法计算'):
            result = add(a, b)
            allure.attach(f'result={result}', '计算结果')
        with allure.step('断言计算结果'):
            assert_that(result, equal_to(5))